#!/bin/sh

set -o errexit
set -o pipefail
set -o nounset

echo "Starting the backend service"

exec "$@"
