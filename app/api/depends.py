from typing import Generator

from db import database


def get_db() -> Generator:
    try:
        db = database()
        yield db
    finally:
        db.close()
