from datetime import date
from urllib import request, error

from fastapi import APIRouter, Depends
from fastapi import HTTPException
from sqlalchemy.orm import Session
import json
from api import depends
from db import exchange_rate_manager
from models import CurrencyConvertReport

rate_router = APIRouter()


@rate_router.get('/', response_model=CurrencyConvertReport)
async def exchange_rate(
        amount: float,
        from_currency: str = "USD",
        to_currency: str = "GBP",
        report_date: date = date.today(),
        db: Session = Depends(depends.get_db),
):
    '''

    The API endpoint takes the following GET query parameters:

    - From currency
    - To currency
    - Date

    And responds with the exchange rate between the two currencies on that particular date.
    The source of the currency exchange data is an external public API, each result we retrieve from the
    external API stored in a local database and upon every request we check if the data is available first in our database
    before requesting the data from the external API.
    :return:
    '''
    data = await exchange_rate_manager.get_exchange_rate(db=db, from_currency=from_currency, to_currency=to_currency,
                                                         report_date=report_date)

    if data:
        return data.exchange(amount=amount)
    else:
        try:
            URL = f"https://api.frankfurter.app/{report_date}?from={from_currency}&to={to_currency}".format_map(
                {"report_date": report_date, "from_currency": from_currency, "to_currency": to_currency})
            req = request.Request(url=URL, method='GET')
            with request.urlopen(req) as f:
                if f.code != 200:
                    raise HTTPException(status_code=404, detail="Not found")
                payload = json.load(f)
            if to_currency in payload['rates']:
                rate = payload['rates'][to_currency]
        except error.HTTPError:
            raise HTTPException(status_code=404, detail="Not found")

        save_data = await exchange_rate_manager.create_exchange_rate(db=db,
                                                                     from_currency=from_currency,
                                                                     to_currency=to_currency,
                                                                     report_date=report_date,
                                                                     rate=rate)
        return save_data.exchange(amount=amount)

    raise HTTPException(status_code=404, detail="Not found")
