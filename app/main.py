'''
The application entry point.

Credit: George Messiha
'''

from api import rate_router
from db import database
from fastapi import FastAPI

app = FastAPI()


app.include_router(rate_router, prefix="/api/v1/rate", tags=["rate"])
