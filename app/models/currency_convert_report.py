from pydantic import BaseModel
from datetime import date


class CurrencyConvertReport(BaseModel):
    from_currency: str
    to_currency: str
    report_date: date
    amount: float
    rate: float
