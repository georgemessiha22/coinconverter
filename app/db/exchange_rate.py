'''
ExchangeRate Model database representing db table. by SQLALchemy.

Credit: George Messiha
'''

from sqlalchemy import (Column, Date, Integer, String, Float)
from sqlalchemy.ext.declarative import declarative_base

Base = declarative_base()


class ExchangeRate(Base):
    __tablename__ = 'exchange_rate'
    id = Column(Integer, autoincrement=True, primary_key=True)
    from_currency = Column(String(50))
    to_currency = Column(String(50))
    report_date = Column(Date)
    rate = Column(Float)

    def __repr__(self):
        return {
            "id": self.id,
            "from_currency": self.from_currency,
            "to_currency": self.to_currency,
            "report_date": self.report_date,
            "rate": self.rate
        }.__str__()

    def exchange(self, amount):
        return {
            "from_currency": self.from_currency,
            "to_currency": self.to_currency,
            "report_date": self.report_date,
            "rate": self.rate,
            "amount": self.rate * amount
        }