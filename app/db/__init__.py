'''
Module responsible for creating tables and CRUD operations interactions with database

Credit: George Messiha
'''
import os

from databases import Database
from sqlalchemy import create_engine, MetaData
from sqlalchemy.orm import sessionmaker

DATABASE_URL = os.getenv('DATABASE_URL')


engine = create_engine(DATABASE_URL)
database = sessionmaker(autocommit=True, autoflush=False, bind=engine)

# from .exchange_rate import ExchangeRate
# from .exchange_rate_manager import get_exchange_rate
