'''
ExchangeRate manager handling the CRUD operations.

Credit: George Messiha
'''

from datetime import date

from sqlalchemy.orm import Session

from .exchange_rate import ExchangeRate


async def get_exchange_rate(db: Session, from_currency: str, to_currency: str, report_date: date):
    """
    Look up for exchange rate.
    """
    return db.query(ExchangeRate).filter_by(
        from_currency=from_currency,
        to_currency=to_currency,
        report_date=report_date
    ).first()


async def create_exchange_rate(db: Session, from_currency: str, to_currency: str, report_date: date, rate: float):
    """
    Create New exchange rate.
    """
    e = ExchangeRate(from_currency=from_currency,
                     to_currency=to_currency,
                     report_date=report_date,
                     rate=rate)

    db.begin()
    db.add(e)
    db.commit()
    return e
