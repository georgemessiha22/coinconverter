# Coin Converter 

A Microservice endpoint that delivers Money Exchange Rate.

## Brief
The API endpoint takes the following GET query parameters:

- From currency
- To currency
- Date

And responds with the exchange rate between the two currencies on that particular date.
The source of the currency exchange data is an external public API, however in order to
minimize the requests we make to that external API, each result we retrieve from the
external API needs to be stored in a local database and upon every request we receive at
the /rate endpoints we need to check if the data is available first in our database before
requesting the data from the external API.

External API to retrieve currency exchange rates https://www.frankfurter.app/

## Installation

### Prerequisites

- docker engine
- docker compose

### Init project
To start the project for the first time run the following in your terminal:
```shell script
docker-compose build -f docker-compose.yaml
docker-compose run backend /setup
docker-compose up -f docker-compose.yaml
```

### Run project
To run the project anytime after first installing.
```shell script
docker-compose up -f docker-compose.yaml
```

## Usage
Swagger docs will be at http://127.0.0.1:8000/docs, you can know all parameters needed for the API.
   

## Notes
Please be aware that this is not a production ready microservice (Just for Demo).


